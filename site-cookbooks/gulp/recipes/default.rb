#
# Cookbook Name:: gulp
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
git "/home/vagrant/.nvm" do
  repository "git://github.com/creationix/nvm.git"
  revision "master"
  user "vagrant"
  group "vagrant"
  action :checkout
end

bash "add nvm" do
  user "vagrant"
  code <<-EOS
    echo "source ~/.nvm/nvm.sh" >> ~/.profile
  EOS
  not_if 'grep "nvm.sh" ~/.profile'
end

bash "install nvm" do
  user "vagrant"
  code <<-EOS
    source ~/.nvm/nvm.sh
    nvm install v0.11.15
    nvm use v0.11.15
    npm install -g gulp
  EOS
  not_if 'which nvm'
end

bash "use nvm" do
  user "vagrant"
  code <<-EOS
    echo "nvm use v0.11.15" >> ~/.profile
  EOS
  not_if 'grep "nvm use" ~/.profile'
end
