#
# Cookbook Name:: base
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
log "start chef"
%w{chkconfig}.each do |pkg|
  package pkg do
    action :install
  end
end
log "end chef"

#timezone
execute "timezone" do
  command "cp -p /usr/share/zoneinfo/Asia/Tokyo /etc/localtime"
  not_if 'grep "JST-9" /etc/localtime'
end

#ntp
cookbook_file "/etc/default/rcS" do
  owner "root"
  group "root"
  source "rcS"
  mode 0644
  not_if 'grep "UTC=no" /etc/default/rcS'
  notifies :run, "execute[update-ntpdate]"
end

execute "update-ntpdate" do
  command "ntpdate #{node['base']['ntp-server']}"
  action :nothing
end

package "ntpdate" do
  action :install
  notifies :run, 'execute[update-ntpdate]', :immediately
end

package "ntp" do
    action :install
end

service "ntp" do
  action [ :enable, :start ]
  supports :status => true, :restart => true, :reload => true
end
