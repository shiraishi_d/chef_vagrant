#
# Cookbook Name:: emacs
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash "install emacs" do
  user "root"
  cwd "/usr/local/src"
  not_if "which emacs"
  code <<-EOH
      wget http://core.ring.gr.jp/pub/GNU/emacs/emacs-24.4.tar.gz
      tar zxvf emacs-24.4.tar.gz
      cd emacs-24.4
      ./configure --without-x
      make
      make install
      EOH
end

dotemacs_dir = "/home/vagrant/.emacs.d"
git dotemacs_dir do
  repository "https://shiraishi_d@bitbucket.org/shiraishi_d/dotemacs.git"
  revision "master"
  user "vagrant"
  group "vagrant"
  action :checkout
  not_if { File.exists?(dotemacs_dir) }
end
