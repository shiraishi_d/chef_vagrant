#
# Cookbook Name:: sinatra_unicorn
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

app_dir = node['sinatra']['app_dir']+'/'+node['project']['name']

directory node['sinatra']['app_dir'] do
  recursive true
  action :create
  owner 'root'
  group 'root'
  mode '0777'
end

git "#{app_dir}" do
  repository "https://shiraishi_d@bitbucket.org/shiraishi_d/ruby_web_template.git"
  revision "master"
  user "vagrant"
  group "vagrant"
  action :export
  not_if { File.exists?("#{app_dir}/.git") }
end

%W{
  #{app_dir}/tmp/pids
}.each do |dir|
  Chef::Log.logger.info "dir:"+dir
  directory dir do
    recursive true
    action :create
    owner node['sinatra']['user']
    group node['sinatra']['group']
    mode '0755'
  end
end

directory node['sinatra']['log_dir'] do
  action :create
  owner node['sinatra']['user']
  group node['sinatra']['group']
  mode '0755'
end

template "#{app_dir}/config.ru" do
  owner node['sinatra']['user']
  group node['sinatra']['group']
  mode '0644'
  not_if { File.exists?("#{app_dir}/config.ru") }
end

template "#{app_dir}/unicorn.rb" do
  owner node['sinatra']['user']
  group node['sinatra']['group']
  mode '0744'
end

execute "bundle install" do
  command "bundle install --path vendor/bundle"
  user node['sinatra']['user']
  cwd app_dir
end

#unicorn init.d script
unicorn_app_service = "unicorn_#{node.set['project']['name']}"

# 起動スクリプトを設置
template "/etc/init.d/#{unicorn_app_service}" do
  source "unicorn_app.erb"
  mode 0755
  not_if "ls /etc/init.d/#{unicorn_app_service}"
  notifies :run, "execute[chkconfig add #{unicorn_app_service}]"
end

# 自動起動設定
execute "chkconfig add #{unicorn_app_service}" do
  action :nothing
  command "chkconfig --add #{unicorn_app_service}"
  notifies :run, "execute[chkconfig #{unicorn_app_service} on]"
  notifies :start, "service[#{unicorn_app_service}]"
end

execute "chkconfig #{unicorn_app_service} on" do
  action :nothing
  command "chkconfig #{unicorn_app_service} on"
end

service "#{unicorn_app_service}" do
  action :nothing
end

service "#{unicorn_app_service}" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end
